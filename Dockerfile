ARG OTP_VERSION=24.0.2
ARG ELIXIR_VERSION=1.12.1
ARG ALPINE_VERSION=3.13.3

FROM hexpm/elixir:$ELIXIR_VERSION-erlang-$OTP_VERSION-alpine-$ALPINE_VERSION AS base-env

WORKDIR /app

RUN mix local.rebar --force && \
    mix local.hex --force

RUN apk add --no-cache \
    git \
    musl-dev
