defmodule CurrencyConverter.RepoCase do
  use ExUnit.CaseTemplate

  alias Ecto.Adapters.SQL.Sandbox

  using do
    quote do
      alias CurrencyConverter.Repo
      alias Ecto.Changeset

      import CurrencyConverter.RepoCase
    end
  end

  setup tags do
    :ok = Sandbox.checkout(CurrencyConverter.Repo)

    unless tags[:async] do
      Sandbox.mode(CurrencyConverter.Repo, {:shared, self()})
    end

    :ok
  end

  # credo:disable-for-next-line Credo.Check.Readability.Specs
  def errors_on(changeset) do
    Ecto.Changeset.traverse_errors(changeset, fn {message, opts} ->
      Regex.replace(~r"%{(\w+)}", message, fn _match, key ->
        opts |> Keyword.get(String.to_existing_atom(key), key) |> to_string()
      end)
    end)
  end
end
