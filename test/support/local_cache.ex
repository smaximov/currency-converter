defmodule LocalCache do
  alias CurrencyConverter.CurrencyRate.Cache
  alias ExUnit.Callbacks

  # credo:disable-for-next-line Credo.Check.Readability.Specs
  def local_cache(_context \\ %{}) do
    pid = Callbacks.start_supervised!({Cache, named_table: false})
    table = Cache.table(pid)

    {:ok, cache: table}
  end
end
