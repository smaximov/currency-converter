defmodule CurrencyConverterTest do
  use CurrencyConverter.RepoCase

  import Mox
  import Tesla.Mock, only: [json: 2]

  setup :verify_on_exit!

  test "convert usd to rub" do
    expect(MockRateLimiter, :check_rate, fn -> :ok end)

    expect(MockTeslaAdapter, :call, fn %{method: :get}, _opts ->
      {:ok, json(%{Valute: %{"USD" => %{Value: 61.9057}}}, status: 200)}
    end)

    assert CurrencyConverter.convert(1.0, "USD", ~D[2020-01-01]) == {:ok, 61.9057}
  end
end
