defmodule CurrencyConverter.CurrencyRateTest do
  use ExUnit.Case, async: true

  import CurrencyConverter.CurrencyRate
  import LocalCache

  import ExUnit.CaptureLog
  import Mox
  import Tesla.Mock, only: [json: 2]

  alias CurrencyConverter.CurrencyRate.Cache

  setup :local_cache
  setup :verify_on_exit!

  describe "fetch_rate/2,3" do
    test "returns {:ok, rate} when rate exists in cache", %{cache: cache} do
      Cache.store(cache, {"USD", ~D[2020-01-01]}, 72.12)

      assert fetch_rate(cache, "USD", ~D[2020-01-01]) == {:ok, 72.12}
    end

    test "returns {:error, :rate_limited} when rate fetching is being rate limited", %{
      cache: cache
    } do
      expect(MockRateLimiter, :check_rate, fn -> {:error, :rate_limited} end)

      assert fetch_rate(cache, "USD", ~D[2020-01-01]) == {:error, :rate_limited}
    end

    test "returns {:ok, rate} when rate is being fetched from API", %{cache: cache} do
      expect(MockRateLimiter, :check_rate, fn -> :ok end)

      expect(MockTeslaAdapter, :call, fn %{method: :get}, _opts ->
        {:ok, json(%{Valute: %{"USD" => %{Value: 61.9057}}}, status: 200)}
      end)

      assert fetch_rate(cache, "USD", ~D[2020-01-01]) == {:ok, 61.9057}
    end

    test "returns {:error, :http} if API returns error", %{cache: cache} do
      expect(MockRateLimiter, :check_rate, fn -> :ok end)

      expect(MockTeslaAdapter, :call, fn %{method: :get}, _opts ->
        {:ok, json(%{Error: "Not found"}, status: 404)}
      end)

      assert fetch_rate(cache, "USD", ~D[2020-01-01]) == {:error, :http}
    end

    test "returns {:error, :task} if rate fetching failed", %{cache: cache} do
      expect(MockRateLimiter, :check_rate, fn -> :ok end)

      expect(MockTeslaAdapter, :call, fn %{method: :get}, _opts ->
        raise RuntimeError
      end)

      assert capture_log(fn ->
               assert fetch_rate(cache, "USD", ~D[2020-01-01]) == {:error, :task}
             end) =~ ~r/RuntimeError/
    end
  end
end
