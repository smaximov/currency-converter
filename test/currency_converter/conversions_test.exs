defmodule CurrencyConverter.ConversionsTest do
  use CurrencyConverter.RepoCase, async: true

  import CurrencyConverter.Conversions

  alias Ecto.Changeset

  describe "get_conversion/3" do
    test "returns nil if there's no past conversion for the given params" do
      refute get_conversion("USD", Date.utc_today(), 1.0)
    end

    test "returns the result of a past conversion" do
      {:ok, _conversion} = save_conversion("USD", Date.utc_today(), 1.0, 72.12)

      assert get_conversion("USD", Date.utc_today(), 1.0) == 72.12
    end
  end

  describe "save_conversion/4" do
    test "when saving duplicate values update result instead" do
      assert {:ok, conversion} = save_conversion("USD", Date.utc_today(), 1.0, 72.12)
      assert conversion.result == 72.12

      assert {:ok, new_conversion} = save_conversion("USD", Date.utc_today(), 1.0, 72.13)

      assert new_conversion.id == conversion.id
      assert new_conversion.result == 72.13

      assert get_conversion("USD", Date.utc_today(), 1.0) == 72.13
    end

    test "returns {:error, changeset} when currency_iso is invalid" do
      assert {:error, %Changeset{} = changeset} =
               save_conversion("", Date.utc_today(), 1.0, 72.12)

      assert errors_on(changeset).currency_iso == ["can't be blank"]

      assert {:error, %Changeset{} = changeset} =
               save_conversion("RU", Date.utc_today(), 1.0, 72.12)

      assert errors_on(changeset).currency_iso == ["should be 3 character(s)"]

      assert {:error, %Changeset{} = changeset} =
               save_conversion("RUBLE", Date.utc_today(), 1.0, 72.12)

      assert errors_on(changeset).currency_iso == ["should be 3 character(s)"]
    end
  end
end
