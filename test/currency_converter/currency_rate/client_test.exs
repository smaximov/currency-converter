defmodule CurrencyConverter.CurrencyRate.ClientTest do
  use ExUnit.Case, async: true

  import Mox
  import Tesla.Mock, only: [json: 2, text: 2]

  import CurrencyConverter.CurrencyRate.Client

  setup :verify_on_exit!

  setup do
    client = new()

    {:ok, client: client}
  end

  describe "by_date/2" do
    test "requests /archive/<DATE>/daily_json.js if given a date in past", %{client: client} do
      expect(MockTeslaAdapter, :call, fn
        %{method: :get, url: "https://www.cbr-xml-daily.ru/archive/2020/01/01/daily_json.js"},
        _opts ->
          {:ok, json(%{Valute: %{"USD" => %{Value: 61.9057}}}, status: 200)}
      end)

      assert by_date(client, ~D[2020-01-01]) == {:ok, %{"USD" => 61.9057}}
    end

    test "requests /daily_json.js if given the current date", %{client: client} do
      expect(MockTeslaAdapter, :call, fn
        %{method: :get, url: "https://www.cbr-xml-daily.ru/daily_json.js"}, _opts ->
          {:ok, json(%{Valute: %{"USD" => %{Value: 61.9057}}}, status: 200)}
      end)

      assert by_date(client, Date.utc_today()) == {:ok, %{"USD" => 61.9057}}
    end

    test "returns {:error, :http} when given a date in future", %{client: client} do
      date = Date.add(Date.utc_today(), 2)

      assert by_date(client, date) == {:error, :http}
    end

    test "returns {:error, :http} if API doesn't return w/ status 200", %{client: client} do
      expect(MockTeslaAdapter, :call, fn %{method: :get}, _opts ->
        {:ok, json(%{Error: "Not found"}, status: 404)}
      end)

      assert by_date(client, Date.utc_today()) == {:error, :http}
    end

    test "returns {:error, :http} if API returns malformed JSON", %{client: client} do
      expect(MockTeslaAdapter, :call, fn %{method: :get}, _opts ->
        {:ok, text("Malformed JSON", status: 200)}
      end)

      assert by_date(client, Date.utc_today()) == {:error, :http}

      expect(MockTeslaAdapter, :call, fn %{method: :get}, _opts ->
        {:ok, json(%{}, status: 200)}
      end)

      assert by_date(client, Date.utc_today()) == {:error, :http}
    end
  end
end
