defmodule CurrencyConverter.CurrencyRate.CacheTest do
  use ExUnit.Case, async: true

  import CurrencyConverter.CurrencyRate.Cache
  import LocalCache

  setup :local_cache

  describe "get_or_store/2,3" do
    test "caches result", %{cache: cache} do
      fun = fn ->
        send(self(), {"USD", ~D[2021-06-26]})

        {:ok, 72.1694}
      end

      assert get_or_store(cache, {"USD", ~D[2021-06-26]}, fun) == {:ok, 72.1694}
      # Cache miss, the function is called:
      assert_receive {"USD", ~D[2021-06-26]}

      assert get_or_store(cache, {"USD", ~D[2021-06-26]}, fun) == {:ok, 72.1694}
      # Cache hit, the function is not called:
      refute_receive {"USD", ~D[2021-06-26]}
    end

    test "different currency results in a different cache entry", %{cache: cache} do
      assert get_or_store(cache, {"USD", ~D[2021-06-26]}, fn ->
               send(self(), {"USD", ~D[2021-06-26]})

               {:ok, 72.1694}
             end) == {:ok, 72.1694}

      assert_receive {"USD", ~D[2021-06-26]}

      assert get_or_store(cache, {"GBP", ~D[2021-06-26]}, fn ->
               send(self(), {"GBP", ~D[2021-06-26]})

               {:ok, 100.366}
             end) == {:ok, 100.366}

      assert_receive {"GBP", ~D[2021-06-26]}
    end

    test "different date results in a different cache entry", %{cache: cache} do
      assert get_or_store(cache, {"USD", ~D[2021-06-26]}, fn ->
               send(self(), {"USD", ~D[2021-06-26]})

               {:ok, 72.1694}
             end) == {:ok, 72.1694}

      assert_receive {"USD", ~D[2021-06-26]}

      assert get_or_store(cache, {"USD", ~D[2021-06-25]}, fn ->
               send(self(), {"USD", ~D[2021-06-25]})

               {:ok, 72.326}
             end) == {:ok, 72.326}

      assert_receive {"USD", ~D[2021-06-25]}
    end
  end

  describe "removing expired entries" do
    test "removes entries created TTL milliseconds ago", %{cache: cache} do
      store(cache, {"USD", ~D[2021-06-26]}, 72.1694)

      assert get(cache, {"USD", ~D[2021-06-26]}) == 72.1694

      # Entries have not yet expired:
      store(cache, {"GBP", ~D[2021-06-26]}, 100.366, now() + 1_000)
      assert sweep(cache) == 0
      assert get(cache, {"USD", ~D[2021-06-26]}) == 72.1694

      # Only one entry has expired:
      assert sweep(cache, now() + ttl()) == 1
      refute get(cache, {"USD", ~D[2021-06-26]})
      assert get(cache, {"GBP", ~D[2021-06-26]}) == 100.366
    end
  end
end
