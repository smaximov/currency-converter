defmodule CurrencyConverter.CurrencyRate.RateLimiter.HammerTest do
  use ExUnit.Case

  import CurrencyConverter.CurrencyRate.RateLimiter.Hammer

  setup do
    Hammer.delete_buckets(buckets())

    :ok
  end

  test "returns {:error, :rate_limited} if per-second bucket limit is exceeded" do
    Hammer.check_rate_inc("cbr-xml-daily:per-second", :timer.seconds(1), 5, 5)

    assert check_rate() == {:error, :rate_limited}
  end

  test "returns {:error, :rate_limited} if per-minute bucket limit is exceeded" do
    Hammer.check_rate_inc("cbr-xml-daily:per-minute", :timer.minutes(1), 120, 120)

    assert check_rate() == {:error, :rate_limited}
  end

  test "returns {:error, :rate_limited} if per-day bucket limit is exceeded" do
    Hammer.check_rate_inc("cbr-xml-daily:per-day", :timer.hours(24), 10_000, 10_000)

    assert check_rate() == {:error, :rate_limited}
  end
end
