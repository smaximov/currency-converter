defmodule CurrencyConverter.Conversions do
  alias CurrencyConverter.Repo

  alias CurrencyConverter.Conversions.ConvertHistory

  @spec get_conversion(String.t(), Date.t(), number()) :: float() | nil
  def get_conversion(currency_iso, %Date{} = date, amount)
      when is_binary(currency_iso) and is_number(amount) do
    case Repo.get_by(ConvertHistory, currency_iso: currency_iso, date: date, amount: amount) do
      nil ->
        nil

      %ConvertHistory{result: result} ->
        result
    end
  end

  @spec save_conversion(String.t(), Date.t(), number(), number()) ::
          Repo.result(ConvertHistory.t())
  def save_conversion(currency_iso, %Date{} = date, amount, result)
      when is_binary(currency_iso) and is_number(amount) and is_number(result) do
    %ConvertHistory{}
    |> ConvertHistory.changeset(%{
      currency_iso: currency_iso,
      date: date,
      amount: amount,
      result: result
    })
    |> Repo.insert(
      returning: true,
      on_conflict: {:replace, ~w[result updated_at]a},
      conflict_target: ~w[currency_iso amount date]a
    )
  end
end
