defmodule CurrencyConverter.Conversions.ConvertHistory do
  use Ecto.Schema

  import Ecto.Changeset

  schema "convert_histories" do
    field :currency_iso, :string
    field :amount, :float
    field :date, :date
    field :result, :float

    timestamps()
  end

  @type t() :: %__MODULE__{
          id: String.t() | nil,
          currency_iso: String.t() | nil,
          amount: float() | nil,
          date: Date.t() | nil,
          result: float() | nil,
          inserted_at: NaiveDateTime.t() | nil,
          updated_at: NaiveDateTime.t() | nil
        }

  @spec changeset(t(), map()) :: Ecto.Changeset.t(t())
  def changeset(%__MODULE__{} = convert_history, attrs) do
    convert_history
    |> cast(attrs, ~w[currency_iso amount date result]a)
    |> validate_required(~w[currency_iso amount date result]a)
    |> unique_constraint(~w[currency_iso amount date]a)
    |> validate_length(:currency_iso, is: 3)
  end
end
