defmodule CurrencyConverter.Repo do
  use Ecto.Repo,
    otp_app: :currency_converter,
    adapter: Ecto.Adapters.Postgres

  @overrides username: "POSTGRES_USER",
             password: "POSTGRES_PASSWORD",
             hostname: "POSTGRES_HOSTNAME",
             url: "DATABASE_URL"

  @type result() :: {:ok, Ecto.Schema.t()} | {:error, Ecto.Changeset.t()}
  @type result(schema) :: {:ok, schema} | {:error, Ecto.Changeset.t(schema)}

  @impl Ecto.Repo
  def init(_context, config) do
    config =
      Enum.reduce(@overrides, config, fn {setting, var_name}, config ->
        if value = System.get_env(var_name) do
          Keyword.put(config, setting, value)
        else
          config
        end
      end)

    {:ok, config}
  end
end
