defmodule CurrencyConverter.Application do
  @moduledoc false

  use Application

  @impl Application
  def start(_type, _args) do
    children = [
      CurrencyConverter.Repo,
      CurrencyConverter.CurrencyRate.Cache,
      {Task.Supervisor, name: CurrencyConverter.CurrencyRate.TaskSupervisor}
    ]

    opts = [strategy: :one_for_all, name: CurrencyConverter.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
