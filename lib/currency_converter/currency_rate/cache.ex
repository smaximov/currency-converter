defmodule CurrencyConverter.CurrencyRate.Cache do
  use GenServer

  @ttl :timer.seconds(3)

  # Delete expired entries each 500 milliseconds:
  @sweep_interval 500

  @default_table __MODULE__
  @default_table_options [:set, :public, read_concurrency: true, write_concurrency: true]

  @type cache_key() :: {currency_iso :: String.t(), date :: Date.t()}
  @type cache_table() :: :ets.tab()

  # Public API:

  defguard is_cache_key(key)
           when is_tuple(key) and tuple_size(key) == 2 and is_binary(elem(key, 0)) and
                  is_struct(elem(key, 1), Date)

  @spec start_link(keyword()) :: GenServer.on_start()
  def start_link(opts \\ []) when is_list(opts) do
    named_table = Keyword.get(opts, :named_table, true)

    GenServer.start_link(__MODULE__, named_table)
  end

  @spec get_or_store(cache_table(), cache_key(), (() -> result)) :: result
        when result: {:ok, float()} | {:error, term()}
  def get_or_store(table \\ @default_table, key, get_callback)
      when is_cache_key(key) and is_function(get_callback, 0) do
    case get(table, key) do
      nil ->
        case get_callback.() do
          {:ok, value} ->
            store(table, key, value, now())

            {:ok, value}

          {:error, reason} ->
            {:error, reason}
        end

      value ->
        {:ok, value}
    end
  end

  # GenServer callbacks:

  @impl GenServer
  def init(named_table) do
    table = :ets.new(@default_table, table_options(named_table))

    schedule_next_sweep(@ttl)

    {:ok, %{table: table}}
  end

  defp table_options(true), do: [:named_table | table_options(false)]
  defp table_options(false), do: @default_table_options

  defp schedule_next_sweep(interval \\ @sweep_interval) do
    Process.send_after(self(), :sweep, interval)
  end

  @impl GenServer
  def handle_info(:sweep, %{table: table} = state) do
    sweep(table)

    schedule_next_sweep()

    {:noreply, state}
  end

  @impl GenServer
  def handle_call(:table, _from, %{table: table} = state) do
    {:reply, table, state}
  end

  # Private API, exposed only for use in tests:

  @doc false
  @spec table(pid()) :: cache_table()
  def table(pid), do: GenServer.call(pid, :table)

  @doc false
  @spec get(cache_table(), cache_key()) :: float() | nil
  def get(table, key) when is_cache_key(key) do
    case :ets.lookup(table, key) do
      [{_key, value, _timestamp}] ->
        value

      [] ->
        nil
    end
  end

  @doc false
  @spec store(cache_table(), cache_key(), float(), integer()) :: float()
  def store(table, key, value, timestamp \\ now())
      when is_cache_key(key) and is_number(value) and is_integer(timestamp) do
    :ets.insert(table, {key, value, timestamp})

    value
  end

  @doc false
  @spec now() :: integer()
  def now, do: System.monotonic_time(:millisecond)

  @doc false
  @spec sweep(cache_table(), integer()) :: non_neg_integer()
  def sweep(table, timestamp \\ now()) do
    :ets.select_delete(
      table,
      # Match specification is based on:
      # ets:fun2ms(fun ({_, _, Created}) when Created + 3000 =< Timestamp -> true end).
      [
        {
          # bind created timestamp
          {:_, :_, :"$1"},
          # guard: created timestamp + ttl <= current timestamp
          [{:"=<", {:+, :"$1", {:const, @ttl}}, {:const, timestamp}}],
          # delete?: true
          [true]
        }
      ]
    )
  end

  @spec ttl() :: integer()
  def ttl, do: @ttl
end
