defmodule CurrencyConverter.CurrencyRate.Client do
  @moduledoc """
  A basic API client for https://www.cbr-xml-daily.ru that can fetch archive data.
  """

  @type t() :: Tesla.Client.t()
  @type option() :: {:adapter, module()}
  @type currency_rates() :: %{String.t() => float()}

  @base_url "https://www.cbr-xml-daily.ru"

  @spec new([option()]) :: t()
  def new(opts \\ []) do
    middleware = [
      {Tesla.Middleware.BaseUrl, @base_url}
    ]

    Tesla.client(middleware, adapter(opts))
  end

  @spec by_date(t(), Date.t()) :: {:ok, currency_rates()} | {:error, atom()}
  def by_date(%Tesla.Client{} = client \\ new(), %Date{} = date) do
    with {:ok, endpoint} <- by_date_endpoint(date),
         {:ok, %Tesla.Env{status: 200, body: body}} <- Tesla.get(client, endpoint),
         {:ok, json} <- Jason.decode(body),
         {:ok, currency_rates} <- parse_json(json) do
      {:ok, currency_rates}
    else
      _error ->
        {:error, :http}
    end
  end

  defp adapter(opts) do
    opts[:adapter] ||
      Application.get_env(:currency_converter, :tesla_adapter, Tesla.Adapter.Hackney)
  end

  defp by_date_endpoint(date) do
    case Date.compare(date, Date.utc_today()) do
      :eq ->
        {:ok, "/daily_json.js"}

      :lt ->
        date = date |> Date.to_string() |> String.replace("-", "/")

        {:ok, "/archive/#{date}/daily_json.js"}

      :gt ->
        :error
    end
  end

  defp parse_json(%{"Valute" => currency_rates}) do
    currency_rates =
      Map.new(currency_rates, fn {currency_iso, %{"Value" => value}} -> {currency_iso, value} end)

    {:ok, currency_rates}
  end

  defp parse_json(_body), do: :error
end
