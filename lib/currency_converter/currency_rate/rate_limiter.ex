defmodule CurrencyConverter.CurrencyRate.RateLimiter do
  @callback check_rate() :: :ok | {:error, :rate_limited}
end
