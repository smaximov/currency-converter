defmodule CurrencyConverter.CurrencyRate.RateLimiter.Hammer do
  @behaviour CurrencyConverter.CurrencyRate.RateLimiter

  @buckets [
    {"cbr-xml-daily:per-second", :timer.seconds(1), 5},
    {"cbr-xml-daily:per-minute", :timer.minutes(1), 120},
    {"cbr-xml-daily:per-day", :timer.hours(24), 10_000}
  ]

  @impl CurrencyConverter.CurrencyRate.RateLimiter
  def check_rate do
    Enum.reduce_while(@buckets, :ok, fn {bucket, scale_ms, limit}, acc ->
      case Hammer.check_rate(bucket, scale_ms, limit) do
        {:allow, _count} ->
          {:cont, acc}

        {:deny, _limit} ->
          {:halt, {:error, :rate_limited}}

        {:error, _reason} ->
          {:halt, {:error, :rate_limited}}
      end
    end)
  end

  # Private API, exposed for use in tests only

  bucket_names = Enum.map(@buckets, &elem(&1, 0))

  @doc false
  @spec buckets() :: [String.t(), ...]
  def buckets, do: unquote(bucket_names)
end
