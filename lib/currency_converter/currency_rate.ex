defmodule CurrencyConverter.CurrencyRate do
  alias CurrencyConverter.CurrencyRate.Cache
  alias CurrencyConverter.CurrencyRate.Client
  alias CurrencyConverter.CurrencyRate.TaskSupervisor

  @spec fetch_rate(Cache.cache_table(), String.t(), Date.t()) :: {:ok, float()} | {:error, atom()}
  def fetch_rate(cache \\ Cache, currency_iso, %Date{} = date) when is_binary(currency_iso) do
    task =
      Task.Supervisor.async_nolink(TaskSupervisor, fn ->
        Cache.get_or_store(cache, {currency_iso, date}, fn ->
          with :ok <- rate_limiter().check_rate(),
               {:ok, currency_rates} <- Client.by_date(date) do
            {:ok, Map.get(currency_rates, currency_iso)}
          end
        end)
      end)

    case Task.yield(task) || Task.shutdown(task) do
      {:ok, result} ->
        result

      {:exit, _} ->
        {:error, :task}

      nil ->
        {:error, :task}
    end
  end

  defp rate_limiter do
    Application.get_env(
      :currency_converter,
      :rate_limiter,
      CurrencyConverter.CurrencyRate.RateLimiter.Hammer
    )
  end
end
