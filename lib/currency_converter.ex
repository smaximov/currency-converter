defmodule CurrencyConverter do
  @moduledoc """
  Documentation for `CurrencyConverter`.
  """

  alias CurrencyConverter.CurrencyRate

  alias CurrencyConverter.Conversions

  @spec convert(amount :: float(), currency_iso :: String.t(), date :: Date.t()) ::
          {:ok, float()} | {:error, atom()}
  def convert(amount, currency_iso, date \\ Date.utc_today())
      when is_float(amount) and is_binary(currency_iso) and is_struct(date, Date) do
    case Conversions.get_conversion(currency_iso, date, amount) do
      nil ->
        case CurrencyRate.fetch_rate(currency_iso, date) do
          {:ok, rate} ->
            result = rate * amount

            Conversions.save_conversion(currency_iso, date, amount, result)

            {:ok, result}

          {:error, error} ->
            {:error, error}
        end

      result ->
        {:ok, result}
    end
  rescue
    _e -> {:error, :error}
  end
end
