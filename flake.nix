{
  description = "A very basic flake providing Elixir development environment";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";

        erlang = pkgs.erlangR24.override {
          enableHipe = false;
        };

        elixir = (pkgs.beam.packagesWith erlang).elixir_1_12;

        elixir-ls = pkgs.elixir_ls.override {
          inherit elixir;
        };
      in {
        devShell = pkgs.mkShell {
          name = "currency_converter";

          buildInputs = [
            elixir
            elixir-ls

            erlang
          ];

          ERL_AFLAGS = "-kernel shell_history enabled";
          TERM = "xterm-256color";
        };
      }
    );
}
