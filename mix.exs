defmodule CurrencyConverter.MixProject do
  use Mix.Project

  def project do
    [
      app: :currency_converter,
      version: "0.1.0",
      elixir: "~> 1.11",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      elixirc_paths: elixirc_paths(Mix.env()),
      dialyzer: dialyzer()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {CurrencyConverter.Application, []}
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:jason, "~> 1.2"},
      {:ecto_sql, "~> 3.4"},
      {:postgrex, ">= 0.0.0"},
      {:tesla, "~> 1.4"},
      {:hackney, "~> 1.17"},
      {:hammer, "~> 6.0"},
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:mix_audit, "~> 0.1", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev, :test], runtime: false},
      {:mox, "~> 1.0", only: :test}
    ]
  end

  defp dialyzer do
    Keyword.merge(
      [
        plt_add_apps: [:mix, :ex_unit, :iex],
        ignore_warnings: ".dialyzer-ignore.exs",
        list_unused_filters: true
      ],
      if System.get_env("CI") do
        [
          plt_core_path: "priv/plts",
          plt_local_path: "priv/plts"
        ]
      else
        []
      end
    )
  end
end
