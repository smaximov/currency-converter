
defmodule CurrencyConverter.Repo.Migrations.AddConvertHistory do
  use Ecto.Migration

  def up do
    create table(:convert_histories) do
      add :amount, :float
      add :currency_iso, :string, size: 3
      add :date, :date
      add :result, :float
      add :metadata, :map

      timestamps()
    end

    flush()

    create index("convert_histories", [:currency_iso, :amount, :date])
    create index("convert_histories", [:metadata, :currency_iso])
  end
end
