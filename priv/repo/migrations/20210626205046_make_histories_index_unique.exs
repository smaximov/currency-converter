defmodule CurrencyConverter.Repo.Migrations.MakeHistoriesIndexUnique do
  use Ecto.Migration

  def up do
    drop index("convert_histories", [:currency_iso, :amount, :date])
    create unique_index("convert_histories", [:currency_iso, :amount, :date])
  end

  def down do
    drop index("convert_histories", [:currency_iso, :amount, :date])
    create index("convert_histories", [:currency_iso, :amount, :date])
  end
end
