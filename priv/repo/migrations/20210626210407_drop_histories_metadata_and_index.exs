defmodule CurrencyConverter.Repo.Migrations.DropHistoriesMetadataAndIndex do
  use Ecto.Migration

  def change do
    drop index("convert_histories", [:metadata, :currency_iso])

    alter table("convert_histories") do
      remove :metadata, :jsonb
    end
  end
end
