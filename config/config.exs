import Config

config :currency_converter, ecto_repos: [CurrencyConverter.Repo]

config :hammer,
  backend: {Hammer.Backend.ETS, expiry_ms: :timer.hours(25), cleanup_interval_ms: :timer.hours(1)}

import_config "#{Mix.env()}.exs"

# Import local config overrides.
overrides = "#{Mix.env()}.local.exs"

if File.exists?(Path.expand(overrides, __DIR__)) do
  import_config overrides
end
